package CraigslistMobileAutomation;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class CraigslistPostPage extends CraigslistBasePage {

	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		AndroidDriver<AndroidElement> driver= Capabilities();
		
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.findElementByLinkText("english").click();

		
		driver.findElementByXPath("//*[@id='ccc']/h4/a/span").click();
		driver.findElementByClassName("com").click();
		
		//Moving back one step:
		driver.pressKeyCode(AndroidKeyCode.BACK);
		
		driver.findElementById("jjj").click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Scrolling to find element
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		   jse.executeScript("window.scrollBy(0,480)", "");
		   
		  driver.findElementByLinkText("human resources").click();
		  driver.pressKeyCode(AndroidKeyCode.BACK);
		  
		

	}

}
