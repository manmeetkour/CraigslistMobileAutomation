package CraigslistMobileAutomation;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class CreatingCraiglistAccount extends CraigslistBasePage {

	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		AndroidDriver<AndroidElement> driver= Capabilities();
		
		driver.findElementByLinkText("english").click();
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("account")));
		
		//Creating An Account
		driver.findElementByLinkText("account").click();
		
		//Entering text in Email field
		driver.findElementById("emailAddress").sendKeys("manmeet@propertyapps.com");
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.findElementByXPath("/html/body/section/section/div/div[2]/form/div[2]/button").click();
		
		
		
		
	}

}
